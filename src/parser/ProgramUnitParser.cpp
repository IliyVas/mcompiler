#include <fstream>
#include <iostream>
#include "ProgramUnitParser.h"

using namespace std;

const char* amToStr(int modifier)
{
    return amString[modifier];
}

const char* utToStr(int utype)
{
    return utString[utype];
}

ProgramUnit ProgramUnitParser::ParseFile(string filename)
{
    ifstream in(filename, std::ios_base::in);
    string storage;

    in.unsetf(std::ios::skipws); // No white space skipping

    copy(
            istream_iterator<char>(in),
            istream_iterator<char>(),
            back_inserter(storage)
    );
    typedef ProgramUnitGrammar<std::string::const_iterator> ProgramUnitGrammar;
    ProgramUnitGrammar unitGrammar; // Our grammar
    ProgramUnit unit;

    using boost::spirit::qi::blank;
    string::const_iterator iter = storage.begin();
    string::const_iterator end = storage.end();
    bool r = qi::phrase_parse(iter, end, unitGrammar, blank, unit);

    if (r && iter == end) {
        cout << "ok";
        return unit;
    }
    else
    {
        cout << "err";
        return unit;
    }
}