#ifndef LANGCOMPILER_PROGRAMUNITPARSER_H
#define LANGCOMPILER_PROGRAMUNITPARSER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <QtCore/qabstractitemmodel.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <boost/spirit/include/phoenix_object.hpp>

namespace fusion    = boost::fusion;
namespace phoenix   = boost::phoenix;
namespace qi        = boost::spirit::qi;
namespace ascii     = boost::spirit::ascii;

//region Enums

enum AccessModifier { MPUBLIC, MPROTECTED, MPRIVATE };
enum ProgramUnitType { UCLASS, UABSTRACT_CLASS, UINTERFACE };

static const char* amString[] = { "public", "protected", "private" };
static const char* utString[] = { "class", "abstract class", "interface" };

const char* amToStr(int modifier);

const char* utToStr(int utype);
//endregion

//region Structs

struct Argument
{
    std::string name;
};

struct Function;
typedef boost::variant<std::string, Function> ArgumentObject;

struct Function
{
    std::string name;
    std::vector<ArgumentObject> objects;
};

struct Method
{
    AccessModifier access_modifier;
    std::string name;
    std::vector<Argument> args;
    std::vector<Function> body;
};

struct ProgramUnit
{
    AccessModifier access_modifier;
    ProgramUnitType unit_type;
    std::string name;
    std::vector<std::string> parents;
    std::vector<Method> methods;
};

class attribute_type;

BOOST_FUSION_ADAPT_STRUCT(
    Argument,
    (std::string, name)
)

BOOST_FUSION_ADAPT_STRUCT(
    Function,
    (std::string, name)
    (std::vector<ArgumentObject>, objects)
)

BOOST_FUSION_ADAPT_STRUCT(
    Method,
    (AccessModifier, access_modifier)
    (std::string, name)
    (std::vector<Argument>, args)
    (std::vector<Function>, body)
)

BOOST_FUSION_ADAPT_STRUCT(
    ProgramUnit,
    (AccessModifier, access_modifier)
    (ProgramUnitType, unit_type)
    (std::string, name)
    (std::vector<std::string>, parents)     //Using of namespace is necessary
    (std::vector<Method>, methods)
)
//endregion

//region Grammar

template <typename Iterator>
struct ProgramUnitGrammar : qi::grammar<Iterator, ProgramUnit(), qi::locals<std::string>, qi::blank_type>
{
    ProgramUnitGrammar() : ProgramUnitGrammar::base_type(unit, "unit")
    {
        using qi::eps;
        using qi::eol;
        using qi::eoi;
        using qi::lit;
        using qi::no_skip;
        using qi::lexeme;
        using qi::repeat;
        using ascii::char_;
        using ascii::string;
        using ascii::alnum;
        using ascii::alpha;
        using qi::on_error;
        using qi::fail;
        using phoenix::construct;
        using phoenix::val;
        using namespace qi::labels;

        using phoenix::at_c;
        using phoenix::push_back;

        accessModifier.add
                ("public",    MPUBLIC)
                ("protected", MPROTECTED)
                ("private",   MPRIVATE)
                ;

        unitType.add
                ("class",     UCLASS)
                ("interface", UINTERFACE)
                ("abstract",  UABSTRACT_CLASS)
                ;

        simpleName %= lexeme[alpha >> *alnum];

        parents %=
                '<'
            >>  (simpleName % ',')  //[push_back(_val), _1)]
        ;

        indentation = no_skip[ (string("  ") | string("    ") | string("\t"))[_val = _1] ];

        tabs = no_skip[ repeat(_r1)[lit('\t')] ];

        argument %= simpleName;

        function %=
                simpleName
            >>  '(' >> object % ',' >> ')'
        ;

        object %= simpleName | function;


        method =
                "def"
            >  accessModifier[at_c<0>(_val) = _1]
            >  simpleName[at_c<1>(_val) = _1]
            >  '(' > argument % ',' > ')' > '=' > eol
            >  *(no_skip[repeat(2)[lit(_r1)]] > function > +(no_skip[eol]))
        ;

        unit =  no_skip[eps]
            >   no_space
            >   accessModifier      [at_c<0>(_val) = _1]
            >   unitType            [at_c<1>(_val) = _1]
            >   simpleName          [at_c<2>(_val) = _1]
            >   (no_skip[ +eol ])[_a += 1]
            >>   -(indentation[_a = _1] >> (method(_a)[push_back(at_c<4>(_val), _1)] % no_skip[ lit(_a) ]))
            >   "end"
            >   eoi
        ;

        no_space = no_skip[!(' ' | lit('\t'))];

        no_space.name("no space in the begining");
        accessModifier.name("Access Mod");
        simpleName.name("Simple name");
        parents.name("Parents");
        tabs.name("Tabs");
        argument.name("Argument");
        function.name("Function");
        object.name("Objects");
        method.name("Method");
        unit.name("unit");

        on_error<fail>
                (
                        unit
                        , std::cout
                          << val("Error! Expecting ")
                          << _4                               // what failed?
                          << val(" here: \"")
                          << construct<std::string>(_3, _2)   // iterators to error-pos, end
                          << val("\"")
                          << std::endl
                );
    }
    qi::rule<Iterator, ProgramUnit(), qi::locals<std::string>, qi::blank_type> unit;
    qi::symbols<char, AccessModifier> accessModifier;
    qi::symbols<char, ProgramUnitType> unitType;
    qi::rule<Iterator, std::string(), qi::blank_type> simpleName;
    qi::rule<Iterator, std::vector<std::string>(), qi::blank_type> parents;
    qi::rule<Iterator, Method(std::string), qi::blank_type> method;
    qi::rule<Iterator, Argument(), qi::blank_type> argument;
    qi::rule<Iterator, Function(), qi::blank_type> function;
    qi::rule<Iterator, ArgumentObject(), qi::blank_type> object;
    qi::rule<Iterator, void(int), qi::blank_type> tabs;
    qi::rule<Iterator, void(), qi::blank_type> no_space;
    qi::rule<Iterator, std::string(), qi::blank_type> indentation;
};
//endregion

class ProgramUnitParser
{
public:
    ProgramUnit ParseFile(std::string filename);
};


#endif //LANGCOMPILER_PROGRAMUNITPARSER_H
