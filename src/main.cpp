#include <iostream>
#include "ApplicativeStructure.h"


int main(int argc, char *argv[])
{
//    QGuiApplication app(argc, argv);
//
//
//
//    ProgramUnit unit;
//    Method foo;
//
//    foo.name = "foo";
//    unit.access_modifier = MPUBLIC;
//    unit.methods.push_back(foo);
//    unit.unit_type = UCLASS;
//
//    StringTreeNode tree = DataConverters::toStringTree(unit);
//
//    StringTreeNodeModel *model = new StringTreeNodeModel(tree);
//
//    cout << tree.data.toStdString() << endl << tree.nodes[3].nodes[0].data.toStdString();
//
//    QQmlApplicationEngine engine;
//    engine.rootContext()->setContextProperty("stringTreeNodeModel", model);
//    engine.load(QUrl(QStringLiteral("qrc:/resources/qml/MainWindow.qml")));
//    return app.exec();

//    ProgramUnitParser parser;
//    try {
//
//
//        auto vr = parser.ParseFile("/home/iliyvas/class_example");
//        cout << " " << amToStr(vr.access_modifier);
//    }
//    catch( const std::exception &e){
//        cout << e.what();
//    }
    using namespace Applicative;
    auto fac = AlgebraicTypesFactory();
    auto a = fac.CreateAlgebraicType("first");
    auto b = fac.CreateAlgebraicType("sec");
    std::cout << a.getIndex();
    return 0;
}
