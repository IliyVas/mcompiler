#ifndef LANGCOMPILER_APPLICATIVESTRUCTURE_H
#define LANGCOMPILER_APPLICATIVESTRUCTURE_H

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>

namespace mi = boost::multi_index;

namespace Applicative {
    auto Apply();

    class AlgebraicTypesFactory
    {
    public:
        struct TypeWithIndex
        {
            int index;
            std::string name;

            TypeWithIndex(int _index, std::string _name) : index(_index), name(_name)
            {
            }
        };

        typedef mi::multi_index_container<
                TypeWithIndex,
                mi::indexed_by<
                        mi::ordered_unique<mi::member<TypeWithIndex, int, &TypeWithIndex::index> >,
                        mi::ordered_unique<mi::member<TypeWithIndex, std::string, &TypeWithIndex::name> >
                >
        >TypeWithIndexSet;

        static TypeWithIndexSet algebraicTypeWithIndexSet;

        template<int index, typename... Types>
        struct IntersectionType
        {
            std::string GetTypeName()
            {
                return std::string("no");
            }
        };

        template<int index, typename... Types>
        struct AlgebraicType {
            int getIndex()
            {
                return index;
            }
        };

        template<typename... Types>
        decltype(AlgebraicType<0, Types...>()) CreateAlgebraicType(std::string name, Types... intersectionTypes);

        template<typename... Types>
        decltype(AlgebraicType<0, Types...>()) CreateAlgebraicType(const int index, Types... intersectionTypes);


    };
}

#endif //LANGCOMPILER_APPLICATIVESTRUCTURE_H
