#include <tuple>
#include "ApplicativeStructure.h"

using namespace std;

template<typename F, typename Tuple, bool Enough, int TotalArgs, int... N>
struct call_impl
{
    auto static call(F f, Tuple&& t)
    {
        return call_impl<F, Tuple, TotalArgs == 1 + sizeof...(N),
                TotalArgs, N..., sizeof...(N)
        >::call(f, std::forward<Tuple>(t));
    }
};

template<typename F, typename Tuple, int TotalArgs, int... N>
struct call_impl<F, Tuple, true, TotalArgs, N...>
{
    auto static call(F f, Tuple&& t)
    {
        return f(std::get<N>(std::forward<Tuple>(t))...);
    }
};

template<typename F, typename Tuple>
auto call(F f, Tuple&& t)
{
    typedef typename std::decay<Tuple>::type type;
    return call_impl<F, Tuple, 0 == std::tuple_size<type>::value,
            std::tuple_size<type>::value
    >::call(f, std::forward<Tuple>(t));
}

using namespace Applicative;
auto Apply() {
    std::make_tuple(1,4);
    return 1;
}

template<typename... Types>
decltype(AlgebraicTypesFactory::AlgebraicType<0, Types...>()) AlgebraicTypesFactory::CreateAlgebraicType(constexpr int index, Types... intersectionTypes)
{
    return AlgebraicType<index, intersectionTypes...>();
}

template<typename... Types>
decltype(AlgebraicTypesFactory::AlgebraicType<0, Types...>()) AlgebraicTypesFactory::CreateAlgebraicType(std::string name, Types... intersectionTypes)
{
    auto set_index = AlgebraicTypesFactory::algebraicTypeWithIndexSet.get<0>();
    auto riter = set_index.rbegin();

    constexpr int new_index;
    if(!AlgebraicTypesFactory::algebraicTypeWithIndexSet.empty())
        new_index = (*riter).index + 1;
    else
        new_index = 0;

    TypeWithIndex new_index_type(new_index, name);
    AlgebraicTypesFactory::algebraicTypeWithIndexSet.insert(new_index_type);

    return AlgebraicTypesFactory::CreateAlgebraicType(new_index, intersectionTypes...);
}



