#ifndef LANGCOMPILER_STRINGTREENODEMODEL_H
#define LANGCOMPILER_STRINGTREENODEMODEL_H

#include <string>
#include <vector>
#include <QObject>
#include <QtCore/qabstractitemmodel.h>
#include <ProgramUnitParser.h>



using namespace std;

struct StringTreeNode
{
    QString data;
    StringTreeNode *parent;
    QList<StringTreeNode> nodes;

    StringTreeNode(QString _data, StringTreeNode *_parent, QList<StringTreeNode> _nodes)
            : data(_data), parent(_parent), nodes(_nodes)
    {
    }

    StringTreeNode(QString _data, StringTreeNode *_parent)
            : data(_data), parent(_parent)
    {
    }
};

class StringTreeNodeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit StringTreeNodeModel(StringTreeNode &data, QObject *parent = 0);
    ~StringTreeNodeModel();

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

private:
    StringTreeNode *rootItem;
    int getRow(StringTreeNode *parentItem) const;
};

namespace DataConverters {
    QList<StringTreeNode> strVectToTreeList(const vector<string> list);
    StringTreeNode toStringTree(const ProgramUnit &unit);
    int a();
}


#endif //LANGCOMPILER_STRINGTREENODEMODEL_H
