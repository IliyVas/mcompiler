#include "StringTreeNodeModel.h"

StringTreeNodeModel::StringTreeNodeModel(StringTreeNode &data, QObject *parent)
        : QAbstractItemModel(parent)
{
    rootItem = &data;
}

StringTreeNodeModel::~StringTreeNodeModel()
{
    //delete rootItem; cause double free
}

QVariant StringTreeNodeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    StringTreeNode *item = static_cast<StringTreeNode*>(index.internalPointer());

    return QVariant(item->data);
}

Qt::ItemFlags StringTreeNodeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant StringTreeNodeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return QVariant(rootItem->data);
}
QModelIndex StringTreeNodeModel::index(int row, int column,
                  const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    StringTreeNode *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<StringTreeNode*>(parent.internalPointer());

    if (row < parentItem->nodes.size())
        return createIndex(row, column, &parentItem->nodes[row]);
    else
        return QModelIndex();
}

int StringTreeNodeModel::getRow(StringTreeNode *parentItem) const
{
    if (parentItem->parent == nullptr) return 0;
    for (int i = 0; i < parentItem->nodes.size(); ++i) {
        if (parentItem == &(parentItem->nodes[i])) return i;
    }
}

QModelIndex StringTreeNodeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    StringTreeNode *childItem = static_cast<StringTreeNode*>(index.internalPointer());
    StringTreeNode *parentItem = childItem->parent;


    if (parentItem == nullptr)
        return QModelIndex();

    return createIndex(getRow(parentItem), 0, parentItem);
}

int StringTreeNodeModel::rowCount(const QModelIndex &parent) const
{
    StringTreeNode *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<StringTreeNode*>(parent.internalPointer());

    return parentItem->nodes.size();
}

int StringTreeNodeModel::columnCount(const QModelIndex &parent) const
{
    return 1;
}

StringTreeNode DataConverters::toStringTree(const ProgramUnit &unit)
{
    StringTreeNode root("root", nullptr);
    QList<StringTreeNode> *_nodes = &root.nodes;

    _nodes->append(StringTreeNode(amToStr(unit.access_modifier), &root));
    _nodes->append(StringTreeNode(utToStr(unit.unit_type), &root));

    StringTreeNode parentsNode("parents", &root);
    QList<StringTreeNode> parentsList;
    for (int i = 0; i < unit.parents.size(); ++i) {
        string str = unit.parents[i];
        parentsList.append(StringTreeNode(QString::fromStdString(str), &parentsNode));
    }
    parentsNode.nodes = parentsList;
    _nodes->append(parentsNode);

    StringTreeNode methodsNode("methods", &root);
    QList<StringTreeNode> methodList;
    for (int i = 0; i < unit.methods.size(); ++i) {
        string name = unit.methods[i].name;
        methodList.append(StringTreeNode(QString::fromStdString(name), &methodsNode));
    }
    methodsNode.nodes = methodList;
    _nodes->append(methodsNode);

    return root;
}

int DataConverters::a()
{
    int i = 1;
    return i + 1;
}