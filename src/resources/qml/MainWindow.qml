import QtQuick 2.3
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import QtQuick.Controls 1.4

Window {
    visible: true
    flags: Qt.FramelessWindowHint
    x: Screen.width / 2 - width / 2
    y: Screen.height / 2 - height / 2
    width: 640
    height: 480

    Rectangle {
        color: "#2a2d33"
        border.color: "black"
        border.width: 1
        anchors.fill: parent

        Text {
            id: logo
            text: qsTr("MCompiler")
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 30
            anchors.leftMargin: 20
            color: "#ffffff"
            font.bold: true
            font.pixelSize: 28
            x: 10
            y: 0

        }

        Image {
            id: close
            anchors.right: parent.right
            anchors.top: parent.top
            source: "qrc:/resources/images/cross.svg"
            sourceSize.width: 15
            sourceSize.height: 15
            anchors.rightMargin: 10
            anchors.topMargin: 10


            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    Qt.quit()
                }
                onEntered: {
                    closeColorEnter.start()
                }
                onExited: {
                    closeColorExit.start()
                }
            }
        }

        TreeView {
            anchors.left: parent.left
            anchors.top: logo.bottom
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.topMargin: 20
            TableViewColumn {
                role: "display"
            }
            model: stringTreeNodeModel
        }

        PropertyAnimation {
            id: closeColorEnter
            target: closeColor
            properties: "color"
            to: "#ff0000"
            duration: 200
        }

        PropertyAnimation {
            id: closeColorExit
            target: closeColor
            properties: "color"
            to: "#ffffff"
            duration: 200
        }

        ColorOverlay {
            id: closeColor
            anchors.fill: close
            source: close
            color: "#ffffff"

        }
    }
}

